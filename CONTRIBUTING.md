#!/bin/bash

command=/usr/bin/htop

if [ -f $command  ]
then
    echo "$command available, lets run it . . ."
else
    echo "$command not available, installing it . . ."
    sudo apt update && sudo apt install -y htop
fi
$command
